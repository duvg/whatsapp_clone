import 'package:flutter/material.dart';

// Import screens 
import 'package:whatsapp_clone/pages/camera_screen.dart';
import 'package:whatsapp_clone/pages/chats_screen.dart';
import 'package:whatsapp_clone/pages/calls_screen.dart';
import 'package:whatsapp_clone/pages/status_screen.dart';


class HomeTabs extends StatefulWidget {
  @override
  _HomeTabsState createState() => new _HomeTabsState();
 }
class _HomeTabsState extends State<HomeTabs> with SingleTickerProviderStateMixin {
  // Properties
  TabController controller;
  @override
  
  void initState() {
    super.initState();
    this.controller = new TabController(length: 4, vsync: this);
  }


  @override
  Widget build(BuildContext context) {
   return new Scaffold(
     appBar: new AppBar(
       title: new Text('WhatsApp'),
       backgroundColor: new Color(0xff075E54),
       bottom: new TabBar(
         tabs: <Widget>[
           new Tab(
              icon: new Icon(Icons.camera_alt)
           ),
           new Tab(
             text: 'CHATS',
           ), new Tab(
             text: 'ESTADOS',
           ),
           new Tab(
             text: 'LLAMADAS',
           )
         ], controller: controller,
       ),
       
     ),
     body: new TabBarView(
       children: <Widget>[
         new CameraScreen(),
         new ChatScreen(),
         new StatuScreen(),
         new CallsScreen()
       ],
       controller: controller,
     ),
   );
  }
}
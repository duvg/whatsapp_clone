class ChatModel 
{
  // Properties
  final  String name;
  String message;
  final String time;
  final String imageUrl;

  // Initialize properties
  ChatModel({this.name, this.message, this.time, this.imageUrl});

}

// Local data to load chats
List<ChatModel> messageData = [
  new ChatModel(
    name: 'Carlos Garcia', 
    message: 'Hola mani como estas',
    time: '03:53 p.m',
    imageUrl: 'https://vignette.wikia.nocookie.net/useravatars/images/4/41/Darius.jpg/revision/latest?cb=20120523224424' 
  ),
  new ChatModel(
    name: 'Juan Craft ', 
    message: 'Saludos! ya viste nuestra nueva imagen',
    time: '04:15 p.m',
    imageUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRqgN2A_OSQKqQCIsWswpPRq402E08wA79g49b4pepiLPf9GV7P' 
  ),
  new ChatModel(
    name: 'Lorena Martinez', 
    message: 'Mañana ire a la piscina',
    time: '04:33 p.m',
    imageUrl: 'https://vignette.wikia.nocookie.net/avatar/images/9/95/Film_-_Yue.png/revision/latest?cb=20121121110806' 
  ),
  new ChatModel(
    name: 'Jose Romero', 
    message: 'Hey que mas',
    time: '05:53 p.m',
    imageUrl: 'https://vignette.wikia.nocookie.net/awatar/images/c/cf/Film_-_Sokka.png/revision/latest?cb=20130926144131' 
  ),
  new ChatModel(
    name: 'Emmanuel Lartra', 
    message: 'Necesito de tu ayuda',
    time: '05:59 p.m',
    imageUrl: 'https://static.giantbomb.com/uploads/original/0/1847/1558920-52211_willsmith_b.jpg' 
  ),
  new ChatModel(
    name: 'Jhon Lars', 
    message: 'Hello, how are you?',
    time: '06:10 p.m',
    imageUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcnSTNmwj336qSSEVJdFv3LapSPIftGujKhPuyyGMKqo4bnI3s' 
  ),

];
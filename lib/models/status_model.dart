import 'package:whatsapp_clone/models/chat_model.dart';

class StatusModel
{
  final String imgUrl;
  final String name;
  final String time;

  StatusModel({this.name, this.time, this.imgUrl});
}

List<StatusModel> status = [
  new StatusModel(
    name: messageData[0].name,
    time: 'Hoy, 7:50 a.m',
    imgUrl: messageData[0].imageUrl
  ),
  new StatusModel(
    name: messageData[1].name,
    time: 'Hoy, 7:50 a.m',
    imgUrl: messageData[1].imageUrl
  ),
  new StatusModel(
    name: messageData[2].name,
    time: 'Hoy, 7:50 a.m',
    imgUrl: messageData[2].imageUrl
  ),
  new StatusModel(
    name: messageData[3].name,
    time: 'Hoy, 7:50 a.m',
    imgUrl: messageData[3].imageUrl
  ),
  new StatusModel(
    name: messageData[4].name,
    time: 'Hoy, 7:50 a.m',
    imgUrl: messageData[4].imageUrl
  ),
  new StatusModel(
    name: messageData[5].name,
    time: 'Hoy, 7:50 a.m',
    imgUrl: messageData[5].imageUrl
  )
];
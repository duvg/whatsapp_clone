import 'package:flutter/material.dart';

class ChatDetailScreen extends StatefulWidget {
  // Properties 
  final String name;

  ChatDetailScreen({this.name});
  @override
  _ChatDetailScreenState createState() => new _ChatDetailScreenState();
 }
class _ChatDetailScreenState extends State<ChatDetailScreen> {
  @override
  Widget build(BuildContext context) {
   return new Container(
     child: new Text(widget.name),
   );
  }
}
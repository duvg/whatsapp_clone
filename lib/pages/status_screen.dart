import 'package:flutter/material.dart';
import 'package:whatsapp_clone/models/status_model.dart';


class StatuScreen extends StatefulWidget {
  @override
  _StatuScreenState createState() => new _StatuScreenState();
 }
class _StatuScreenState extends State<StatuScreen> {
  
  @override
  Widget build(BuildContext context) {
   final _width = MediaQuery.of(context).size.width;


   return new Column(
     children: <Widget>[
       new Container(
         width: _width,
         child: new ListTile(
          leading: new CircleAvatar(
            child: new Icon(Icons.check),
            backgroundColor: Colors.blueAccent,
          ),
          title: new Text('Duviel Garcia'),
          subtitle: new Text('Agregar estado'),
         )
       ),
       new Container(
         width: _width,
         padding: const EdgeInsets.all(5.0),
         color: Colors.grey[200],
         child: new Text(
           'Recientes',
           style: new TextStyle(color: Colors.grey[700], fontWeight: FontWeight.bold) 
         ),
       ), 
       new Expanded(
         child: new ListView.builder(
           itemCount: status.length,
           itemBuilder: (context, i) => new Column(
             children: <Widget>[
               new Divider( height: 10.0,),
               new ListTile(
                 leading: new CircleAvatar(
                   backgroundImage: new NetworkImage(status[i].imgUrl),
                 ),
                 title: new Text(status[i].name),
                 subtitle: new Text(status[i].time),
               )
             ],
           ),
         ),
       )
     ],
   );
  }
}

/*new Text(
                  'Recientes',
                  style: new TextStyle(color: new Color(0xff075E54), fontWeight: FontWeight.bold),
                ),*/
import 'package:flutter/material.dart';
import 'package:whatsapp_clone/home_tabs.dart';

void main() {
  runApp( new MaterialApp(
    home: HomeTabs()
  ));
}

class Tabs extends StatelessWidget {
 @override
 Widget build(BuildContext context) {
  return new MaterialApp(
    title: "Whatsapp",
    theme: new ThemeData(
      primaryColor: new Color(0xff075E54),
      accentColor: new Color(0xff25D366)
    ),
    debugShowCheckedModeBanner: false,
    home: new HomeTabs(),
 );
 }
}